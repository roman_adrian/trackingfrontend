<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\FindTrackingDateFormType;
use AppBundle\Service\GetTrackingDateService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class MainController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request):Response
    {
        /** @var Form $form */
        $form = $this->createForm(FindTrackingDateFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            return $this->redirectToRoute('result',['requestId'=>$data['trackingId']]);
        }

        return $this->render('TrackingViews/index.html.twig',[
            'search_form'=>$form->createView()
        ]);
    }


    /**
     * @Route("/result/{requestId}", name="result")
     * @param Request $request
     * @param string $requestId
     * @param GetTrackingDateService $fetchDataFromServer
     * @return Response
     */
    public function responseAction (Request $request, string $requestId, GetTrackingDateService $fetchDataFromServer):Response
    {
        /** @var array $data */
        $data = $fetchDataFromServer->getOrder($requestId);

        if (null == $data){
            //there was a problem with the request
            return $this->render('TrackingViews/result.html.twig',[
                'result'=>'error'
            ]);
        }

        return $this->render('TrackingViews/result.html.twig',[
            'result'=>$data
        ]);
    }
}
