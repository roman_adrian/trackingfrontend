<?php

namespace AppBundle\Service;

use DateTime;
use Exception;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

class GetTrackingDateService
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $serverUrl;

    /**
     * @param string $serverUrl
     * @param LoggerInterface $logger
     */
    public function __construct(string $serverUrl, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->serverUrl = $serverUrl;
    }

    /**
     * @param string $requestId
     * @return array|null
     */
    public function getOrder(string $requestId): ?array
    {
        /** @var Client $client */
        $client = new Client([
            'base_uri' => $this->serverUrl,
            'defaults'=>[
                'exceptions'=>false
            ]
        ]);

        try {
            /** @var string $response */
            $response = $client->get('/order/' . $requestId);

            /** @var int $statusCode */
            $statusCode = $response->getStatusCode();
            if (200 == $statusCode) {
                /** @var array $data */
                $data = json_decode($response->getBody(), true);
                try {
                    $data['shipping_date'] = (new DateTime($data['shipping_date']))->format('Y-m-d');
                } catch (Exception $e) {
                    $this->logger->error('Error parsing the date from the json: ' .
                        $response->getBody() . ' error ' . $e->getMessage());
                }
                return $data;
            } else {
                if (400 == $statusCode) {
                    //we have requested an item that does not exist
                    $this->logger->warning('Item ' . $requestId . ' was not found on the Server');
                    return null;
                }
            }
            //unknown exception
            $this->logger->error('Item ' . $requestId . ' returned the error ' . $response->getBody());
        } catch (Exception $e){
            $this->logger->error('Item ' . $requestId . ' returned the error ' . $e->getMessage());
        }
        return null;
    }

}