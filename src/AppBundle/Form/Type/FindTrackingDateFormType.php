<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;

class FindTrackingDateFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('trackingId',TextType::class,[
                'label' => 'Tracking Id',
                'required'=> true,
                'constraints' => [new Length(['min' => 1])]
            ])
            ->add('search',SubmitType::class)
            ;
    }
}